class Meaning:
    self.image # Picked by the user
    self.definition # In target language, simplified (take from kid's dictionary) 
    self.synonyms
    self.antonyms
    self.example_sentence = ExampleSentence

class Spelling:
    self.spelling # How the work is written

class Pronunciation:
    self.phonetic_alphabet # Optional
    self.accent # For languages that accentuate the pronunciation
    self.sound # Taken from sound bank, recorded by natives

class ExampleSentence:
    self.complete
    self.fill_in_the_blank

class Word:
    self.meaning = Meaning
    self.pronunciation = Pronunciation
    self.spelling = Spelling

class Sentence:
    self.words = [Word]

class WordFrequencyList:
    self.words = [Word]
    self.string
    def next():
        return next_card

class SentenceFrequencyList:
    self.sentences = [Sentence]

class WordReviewQueue:
    self.queue = [Word]


class User:
    def __init__():
        self.word_frequency_list = WordFrequencyList()
        self.created_words = [Word]
    def pick_image():

    def create_word_card():
        new_word = word_frequency_list.next()
        new_word.meaning.image = pick_image()

    def review_card():

class Interface:
    def login():

    def pick_image():
        return image

    def view_word():
        reviewed_word = queue.next()

    def 

class AnswerCard:

class MeaningCard:
